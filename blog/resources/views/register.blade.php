<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h2>Sign Up Form</h2>
    <form action="Pendaftaran.php" method="POST">
        <fieldset>
            <legend>Pendaftaran</legend>
            <p>
                <label>First Name:</label>
                <input type="text" name="First Name" placeholder="First Name...."/>
            </p>
            <p>
                <label>Last Name:</label>
                <input type="text" name="Last Name" placeholder="Last Name...."/>
            </p>
            <p>
                <label>Gender:</label><br><br>
                <label><input type="radio" name="Gender" value="Male"/> Male</label><br>
                <label><input type="radio" name="Gender" value="Female"/> Female</label><br>
                <label><input type="radio" name="Gender" value="Other"/> Other</label><br>
            </p>
            <p>
                <label>Nationality:</label>
                <select name="Nationality">
                <option value="indonesia">Indonesia</option>
                <option value="inggris">Inggris</option>
                <option value="singapura">Singapura</option>
                <option value="japan">Japan</option>
                <option value="south korea">South Korea</option>
            </p>
            <p>
                <label for="Language">Language Spoken:</label>
                <select name="Language" id="Language"></select>
                <input type="checkbox"name="Language" value="Bahasa Indonesia">
                <label for="Language"> Bahasa Indonesia</label><br>
                <input type="checkbox"name="Language" value="English">
                <label for="Language"> English</label><br>
                <input type="checkbox"name="Language" value="Other">
                <label for="Language"> Other</label><br>
                </select>
            </p>
            <p>
                <label>Bio:</label>
                <textarea name="bio"></textarea>
            </p>
            <p>
                <input type="submit" name="submit" value="Sign Up" />
            </p>
        </fieldset>
    </form>

</body>
</html>